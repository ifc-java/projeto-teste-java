package main.java.com.ifc;

class Metodo3{
    String nome = "João Silva";

    public String retornaNome()
    {
        return nome;
    }
}

public class MetodoComRetorno {

    public static void main(String[] args) {

        Metodo3 m3 = new Metodo3();
        System.out.println(m3.retornaNome());

    }

}
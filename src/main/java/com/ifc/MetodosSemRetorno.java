package main.java.com.ifc;

class Metodo2{

    public void escrever()
    {
        System.out.println("Método sem Retorno - VOID ");
    }
}

public class MetodosSemRetorno {

    public static void main(String[] args) {

        Metodo2 m = new Metodo2();
        m.escrever();
    }
}